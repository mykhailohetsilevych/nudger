#!/usr/bin/env bash


# check if git repo
if git rev-parse --is-inside-work-tree
then
  echo .git
else
    echo "you must be in the top-level of a git repo"
    echo "to create a new Git repository: 'git init'"
    exit 1;
fi;


# pwd
# move to top level of git repo
while [[ ! -d .git ]] ; do
    cd ..
done

# pwd

# check if top level of git repo
if [[ $(git rev-parse --show-toplevel) == $(pwd) ]]
then
    echo 'you are at the top-level of a git repo';
else
    echo "you must be in the top-level of a git repo"
    exit 1;
fi

# pwd
# install if needed pre-commit
pre-commit --version || pip install pre-commit
pip install --upgrade pre-commit
pre-commit autoupdate

# ggshield --version || pip install ggshield

# check if .pre-commit-config.yaml exists -if not add file
# to prevent duplicate entries
FILE=.pre-commit-config.yaml
if [ -f "$FILE" ]; then
    echo "$FILE exists."
else
  # pre-commit sample-config >> .pre-commit-config.yaml
  # wget https://gitlab.com/tangibleai/team/-/raw/test-pre-commit/.pre-commit-config.yaml
  wget https://pre-commit-config.nyc3.digitaloceanspaces.com/.pre-commit-config.yaml
fi


# Create a global git commit hook
# Enable git templates:
git config --global init.templatedir '~/.git-templates'

#This tells git to copy everything in ~/.git-templates to your per-project .git/ directory when you run git init
mkdir -p ~/.git-templates/hooks

# copy pre-commit hook to ~/.git-templates/hooks/pre-commit
wget -O ~/.git-templates/hooks/pre-commit https://gist.githubusercontent.com/saliceti/7eb0ba0bb5ed875df515/raw/24222f69640f92bab47b43806fc9044450ccba0f/pre-commit --quiet

#Make sure the hook is executable.
chmod a+x ~/.git-templates/hooks/pre-commit

# Re-initialize git in each existing repo you'd like to use this in:
git init

pre-commit install
