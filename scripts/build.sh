#!/usr/bin/env bash

if [ -f ".env" ] ; then
    source .env
fi

if [ -f ".venv/bin/activate" ] ; then
    source .venv/bin/activate
fi

pip install --upgrade pip poetry virtualenv wheel
pip install -e .
#source .env
pip install -r requirements.txt

python manage.py migrate

python manage.py shell <<EOF
print('Creating a superuser')

import os
from django.db.utils import IntegrityError
from django.contrib.auth.models import User

username = os.environ["DJANGO_SUPERUSER_USERNAME"]
password = os.environ["DJANGO_SUPERUSER_PASSWORD"]

try:
    User.objects.create_superuser(
	      username = username,
	      password = password,
)
    print(f"User {username} has been created")
    print(f'With a length password: {len(password)}')
except IntegrityError:
    print("User already exists!")
except Exception as err:
    print(err)
exit()
EOF

echo "Collecting static files!"
python manage.py collectstatic --noinput -v 3
