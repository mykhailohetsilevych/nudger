from celery import shared_task
from celery.utils.log import get_task_logger
from celery.schedules import crontab
from django_celery_beat.models import PeriodicTask, ClockedSchedule, IntervalSchedule, CrontabSchedule

import json
import string

from .email import send_email_nudge
from .sms import send_sms_nudge

from django.contrib.auth.models import User
from django.utils.crypto import get_random_string

from datetime import datetime


@shared_task
def create_random_user_accounts(total):
    for i in range(total):
        username = 'user_{}'.format(get_random_string(10, string.ascii_letters))
        email = '{}@example.com'.format(username)
        password = get_random_string(50)
        User.objects.create_user(username=username, email=email, password=password)
    return '{} random users created with success!'.format(total)


logger = get_task_logger(__name__)


@shared_task(name="email_nudge")
def email_nudge(name, email, body):
    logger.info("Sent review email")

    # return send_email_nudge(name, email, body)
    return send_email_nudge(name, email, body)


# Sends an immediate SMS nudge
@shared_task(name="sms_nudge")
def sms_nudge(name, phone, body):
    logger.info("Sent review email")
    return send_sms_nudge(name, phone, body)


# Converts a datetime form result to cron parameters
def datetimeToCron(datetime):
    minute = datetime.minute
    hour = datetime.hour
    day_of_week = "*"
    day_of_month = datetime.day
    month_of_year = datetime.month

    cron_sch = CrontabSchedule.objects.create(
        minute=minute,
        hour=hour,
        day_of_week=day_of_week,
        day_of_month=day_of_month,
        month_of_year=month_of_year,
        timezone="US/Mountain"
    )

    return cron_sch


# Schedules a one-time email nudge based on scheduled datetime
@shared_task(name="crontab_nudge")
def crontab_email_nudge(name, email, body, time):
    cron_sch = datetimeToCron(time)

    PeriodicTask.objects.create(
        crontab=cron_sch,
        task="email_nudge",
        name=f"Crontab task for {name} at {time}",
        args=json.dumps([name, email, body]),
        one_off=True,
    )


# Schedules a one-time SMS nudge based on scheduled datetime
@shared_task(name="crontab_sms_nudge")
def crontab_sms_nudge(name, phone, body, time):

    cron_sch = datetimeToCron(time)

    PeriodicTask.objects.create(
        crontab=cron_sch,
        task="sms_nudge",
        name=f"Crontab task for {name} at {time}",
        args=json.dumps([name, phone, body]),
        one_off=True,
    )
