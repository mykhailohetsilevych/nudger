# Sendgrid imports
import os
from pathlib import Path
from sendgrid import SendGridAPIClient
from sendgrid.helpers.mail import Mail
from nudger.settings import SENDGRID_API_KEY


# Mail through Sendgrid
def send_email_nudge(name, email, body):

    body = body
    subject = name
    to_emails = [email]

    message = Mail(
        from_email="engineering+tanbot@tangibleai.com",
        to_emails=to_emails,
        subject=subject,
        plain_text_content=body,
    )

    try:
        sg = SendGridAPIClient(SENDGRID_API_KEY)
        response = sg.send(message)
        print("SENDGRID RESPONSE")
        print(response.status_code)
        print(response.body)
        print(response.headers)

    except Exception as e:
        print("Sendgrid Error")
        print(e)


if __name__ == "__main__":
    SENDGRID_API_KEY = os.environ.get("SENDGRID_API_KEY")
    REPO_DIR = Path(__file__).parent.parent.parent
    print(REPO_DIR)
    DATA_DIR = Path(REPO_DIR, "data")
    print(DATA_DIR)
