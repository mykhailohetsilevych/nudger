import os
from dotenv import load_dotenv
from twilio.rest import Client

load_dotenv()

account_sid = os.environ["TWILIO_ACCOUNT_SID"]
auth_token = os.environ["TWILIO_AUTH_TOKEN"]
client = Client(account_sid, auth_token)
from_number = os.environ["TWILIO_DEFAULT_NUMBER"]
to_number = "+16178521426"

# send a regular message
message_body = "Python regular SMS test"
message = client.messages.create(
    body=message_body, from_=from_number, to=to_number)

# send a Twilio Studio flow
flow_sid = "FWcbcd3ca951390753a6816059feba070e"
execution = client.studio.v1.flows(flow_sid).executions.create(
    to=to_number, from_=from_number
)
