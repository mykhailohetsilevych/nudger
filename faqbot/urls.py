from django.urls import path
from faqbot.views import FaqCreateView, FaqListView, math_question, student_question

urlpatterns = [
    path('', FaqListView.as_view(), name="faq-list"),
    path('new/', FaqCreateView.as_view(), name="faq-create"),
    path('math-question/', math_question, name='math-question'),
    path('student-question/', student_question, name='student-question'),
]
