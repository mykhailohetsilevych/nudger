from django.contrib import admin
from faqbot.models import Faq, MathQuestion, StudentQuestion

# Register your models here.
admin.site.register(Faq)
admin.site.register(MathQuestion)
admin.site.register(StudentQuestion)
