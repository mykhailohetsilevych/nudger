#!/usr/bin/env sh

export MY_SERVER_NAME=$(curl ifconfig.me)

envsubst "$${MY_SERVER_NAME}" < etc/nudger_gunicorn.nginx > etc/envsubst_nudger_gunicorn.nginx
