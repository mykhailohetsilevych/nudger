import uuid

from django.db import models

MESSAGE_DIRECTION_CHOICES = [
    ('inbound', 'inbound'),  # HL: added
    ('outbound', 'outbound'),
    ('unknown', 'unknown'),
]

# FIXME: add _CHOICES 
SENDER_TYPE = [
    ('user', 'user'),
    ('chatbot', 'chatbot'),
    ('human agent', 'human agent'),
]

LANGUAGE_CHOICES = [
    ('en', 'en'),
    ('es', 'es'),
    ('zh-hans', 'zh-hans'),
    ('zh-hant', 'zh-hant')
]


class Document(models.Model):
    file = models.FileField(upload_to='documents/')
    convo_name = models.CharField(max_length=255, blank=True)
    description = models.CharField(max_length=255, blank=True)
    created_on = models.DateTimeField(auto_now_add=True)


class ConversationTracker(models.Model):
    student_id = models.CharField(null=False, max_length=100)
    state_name = models.CharField(max_length=100, blank=True, null=True)
    updated_on = models.DateTimeField(auto_now=True)


class Convo(models.Model):
    name = models.CharField(null=False, max_length=100, unique=True)
    description = models.CharField(max_length=100, blank=True, null=True)


class Checkpoint(models.Model):
    created_on = models.DateTimeField(auto_now_add=True, editable=False)
    description = models.CharField(max_length=100, blank=True, null=True)
    published = models.BooleanField(default=False)
    convo = models.ForeignKey(Convo, on_delete=models.CASCADE)
    # HL: want a JSONField to store things like intended audience, grade level, conversation goal/purpose, etc
    # meta = models.JSONField(null=True)


class State(models.Model):
    # Foreign key to itself
    checkpoint = models.ForeignKey(Checkpoint, on_delete=models.CASCADE)

    next_states = models.ManyToManyField(
        "self",
        through="Trigger",
        through_fields=("from_state", "to_state")
    )
    state_name = models.CharField(
        null=False,
        max_length=255,
        help_text="The title the system uses to recognize the state"
    )
    level = models.IntegerField(
        null=True,
        help_text="An optional field that tells the depth of the action"
    )

    # HL: should be in the Trigger table because they are defined by what the user has said?
    update_args = models.JSONField(null=True)
    update_kwargs = models.JSONField(null=True)
    update_context = models.JSONField(
        null=True,
        help_text="May hold information such as gender, location, or history of previous exchanges"
    )
    # HL: What creates the association between what the bot says and a state name/id ?


class Message(models.Model):
    """ A table for messages the bot can send.
    Each row contains one message written in one language.
    """
    sequence_number = models.IntegerField(blank=False, null=False)
    state = models.ForeignKey(
        State,
        null=False,
        blank=False,
        on_delete=models.CASCADE
    )
    bot_text = models.TextField(
        null=False,
        help_text="The message of the state written in English"
    )
    lang = models.CharField(choices=LANGUAGE_CHOICES, default='en', max_length=100)


class Trigger(models.Model):
    """ A table of supported user inputs that connect two states.
    Users can enter the intent_text by button click or text input.
    Each row is one trigger option written in one language.
    """
    # Foreign Keys
    from_state = models.ForeignKey(
        State,
        related_name="rel_from_state",
        on_delete=models.CASCADE,
        null=True,
        blank=True
    )
    to_state = models.ForeignKey(
        State,
        related_name="rel_to_state",
        on_delete=models.CASCADE,
        null=True,
        blank=True
    )
    intent_text = models.CharField(
        null=False,
        max_length=255,
        help_text="The user utterance that triggers a transition to a new bot state."
    )
    update_args = models.JSONField(null=True)
    update_kwargs = models.JSONField(null=True)
    # HL: we should probably have an nlp = CharField(null=True) to define the NLP algorithm used to recognize user intents (Trigger messages)
    lang = models.CharField(
        choices=LANGUAGE_CHOICES,
        default='en',
        max_length=100
    )
    is_button = models.BooleanField(default=False)


# No table needs the word "Data" to describe what it does, please rename as `Interaction()`
class InteractionData(models.Model):
    """ A table that logs data each time a message is sent """
    message_id = models.UUIDField(
        default=uuid.uuid4,
        unique=True,
        help_text="A unique id assigned to each message that is sent out"
    )
    message_direction = models.CharField(
        choices=MESSAGE_DIRECTION_CHOICES,
        #        default=MESSAGE_DIRECTION_CHOICES[-1][0]  # HL: suggestion to ensure it's a valid CHOICE
        default='en',  # FIXME: en is not a valid MESSAGE_DIRECTION_CHOICES
        max_length=100
    )
    message_text = models.TextField(
        null=False,
        help_text="The message of the state"
    )
    send_time = models.DateTimeField(
        auto_now_add=True,
        help_text="The time that the message was sent (as opposed to scheduled)"
    )
    state = models.ForeignKey(
        State,
        null=True,
        max_length=255,
        on_delete=models.SET_NULL,
        help_text="The state of the chatbot when a message is sent"
    )
    sender_type = models.CharField(
        choices=SENDER_TYPE,
        max_length=20
    )
    user_id = models.CharField(
        null=True,
        max_length=255,
        help_text="A student or user ID that can be mapped to the User.pk OR an anonymous ID that is unique for a single session (room)"
    )
    bot_id = models.CharField(
        default="Poly Chatbot",
        max_length=255,
        help_text="The name or id of the chatbot being interacted with"
    )
    channel = models.CharField(
        default="WSNYC Website",
        max_length=128,
        help_text="The platform the user interacted with, such as WSNYC website"
    )

    """
    TODO Questions:
    * Should I incorporate status messages (like errors into this)?
    * For the state, should I return the state object or just the name
    """
