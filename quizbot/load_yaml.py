from django.core.exceptions import ObjectDoesNotExist

from quizbot.bot_manager import StateManager
from quizbot.make_dialog import coerce_dialog_tree_series
from quizbot.models import State, Message, Trigger, Checkpoint, Convo

langs = StateManager.lang_dict.values()


def get_or_create_convo(name, description=None):
    """Creates or updates active bot version"""
    try:
        convo = Convo.objects.get(name=name)
        return convo
    except ObjectDoesNotExist:
        convo = Convo(name=name, description=description)
        convo.save()
        return convo


def unpublish_checkpoints(convo):
    checkpoints = Checkpoint.objects.filter(convo=convo, published=True)
    for checkpoint in checkpoints:
        checkpoint.published = False
        checkpoint.save()


def create_flow_data(file_path, convo_name, description):
    """Creates flow data and updates active bot version"""

    convo = get_or_create_convo(convo_name)
    checkpoint = Checkpoint.objects.filter(convo=convo, description=description).last()

    if checkpoint:
        unpublish_checkpoints(convo)
        checkpoint.published = True
        checkpoint.save()
        return f"Checkpoint already exists, status has been changed to published."

    DIALOG_TREE = coerce_dialog_tree_series(file_path)
    dialog_ = {node['name']: node for node in DIALOG_TREE}

    checkpoint_new = Checkpoint(
        convo=convo,
        description=description,
    )
    checkpoint_new.save()

    for name in dialog_:
        state = State(
            state_name=name,
            checkpoint=checkpoint_new,
        )
        state.save()

        for lang in langs:
            # Makes an entry in the Message table for each English message of a State
            for sequence_number, message in enumerate(dialog_[name].get("actions", {}).get(lang, "")):
                message = Message(
                    state=state,
                    sequence_number=sequence_number,
                    bot_text=message,
                    lang=lang,
                )
                message.save()

    for name in dialog_:
        try:
            state = State.objects.get(
                state_name=name,
                checkpoint=checkpoint_new,
            )
        except State.DoesNotExist:
            checkpoint_new.delete()
            message = f"The state_name: {name} does not exist!"
            return f"Checkpoint creation failed!\n{message}"

        for lang in langs:
            # TODO: See if this try can be simplified to test whether there is a langauge represented and then take the stuff if it is or pass if it isn't

            try:
                functions_to_call = dict(dialog_[name].get("triggers", {}).get("functions", "").items())
            except:
                functions_to_call = None

            try:
                buttons = list(dialog_[name].get("buttons", {}).get(lang, "").items())
            except AttributeError:
                buttons = list(dialog_[name].get("buttons", {}).get(lang, ""))

            try:
                triggers = list(dialog_[name].get("triggers", {}).get(lang, "").items())
            except AttributeError:
                triggers = list(dialog_[name].get("triggers", {}).get(lang, ""))

            for is_button, item_list in enumerate([triggers, buttons]):
                for intent_text, to_state_name in item_list:
                    try:
                        to_state = State.objects.get(state_name=to_state_name, checkpoint=checkpoint_new)
                    except State.DoesNotExist:
                        checkpoint_new.delete()
                        message = f"For intent_text: {intent_text} state_name: {name} to_state_name: {to_state_name}' does not exist!"
                        return f"Checkpoint creation failed!\n{message}"

                    trigger = Trigger(
                        from_state=state,
                        to_state=to_state,
                        intent_text=intent_text,
                        update_kwargs=functions_to_call,
                        lang=lang,
                        is_button=is_button,
                    )
                    trigger.save()

    unpublish_checkpoints(convo)
    checkpoint_new.published = True
    checkpoint_new.save()

    return f"Checkpoint has been created and published!"
