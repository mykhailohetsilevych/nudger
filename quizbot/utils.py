import yaml
from yaml.loader import SafeLoader


def get_keys_from_yaml(file_name):
    with open('countByOne_0003.yml') as f:
        data = yaml.load(f, Loader=SafeLoader)

    convo_name = data[0].get("convo_name")
    convo_description = data[0].get("convo_description")
    nlp = data[0].get("nlp")

    return {
        "convo_name": convo_name,
        "convo_description": convo_description,
        "nlp": nlp
    }
