from random import choice

import markdown
from django.core.exceptions import ObjectDoesNotExist

from quizbot.models import Convo, ConversationTracker, Checkpoint, State


class ConversationManager:

    @staticmethod
    def get_success_messages():
        lst = ["Exactly right!", "Yes!", "Well done!", "Nice job.", "Good work!", "Fantastic!", "Amazing!",
               "Excellent!", "Nice one!", "You got it!", "Correct!", "Super!", "You are a star!", "Great work!",
               "Great job!", "Right on.", "Great job, you got it!", "Perfect!"]
        return choice(lst)

    @staticmethod
    def get_failure_messages():
        lst = ["Oops!  Looks like a mistake.  Can you try again?", "Not quite.  Please try one more time.",
               "Oops!  That's not right.  Try it again.", "Give it another try, okay?",
               "Oh oh!  That's not right.  Give it another try.", "Look carefully and try again.",
               "Almost. Try it again."]
        return choice(lst)

    @staticmethod
    def get_bot(bot_name):
        """Returns the bot from the botname"""
        try:
            bot = Convo.objects.get(name=bot_name)
        except ObjectDoesNotExist:
            return
        else:
            return bot

    @staticmethod
    def get_trace(student_id):
        """Returns trace if exists else creates"""
        try:
            trace = ConversationTracker.objects.get(student_id=student_id)
        except ObjectDoesNotExist:
            trace = ConversationTracker(student_id=student_id, state_name=None)
            trace.save()
        finally:
            return trace

    @staticmethod
    def update_trace(trace, state_name):
        """Updates student trace data"""
        trace.state_name = state_name
        trace.save()

    @staticmethod
    def get_conversation_flow(bot, version):
        """Get active conversation flow for the bot"""
        conversation_flow = Checkpoint.objects.get(
            bot=bot,
            version=version
        )
        return conversation_flow

    @staticmethod
    def update_state(conversation_flow, trace, user_text, message):
        # TODO remove additional
        """Checks the user answer and iterates to the next state"""
        data = {'state_name': trace.state_name, 'user_text': user_text, 'context': {'lang': 'en'}}
        state = State.get_current_state_from_database(data, conversation_flow)

        if message.lower() == state["additional"]["correct_answer"]:
            bot_text = markdown.markdown(ConversationManager.get_success_messages(), extensions=['attr_list'])
        else:
            bot_text = markdown.markdown(ConversationManager.get_failure_messages(), extensions=['attr_list'])

        next_state = next(item["to_state"] for item in state["triggers"] if item.get("to_state"))
        data = {'state_name': next_state, 'user_text': user_text, 'context': {'lang': 'en'}}
        state = State.get_current_state_from_database(data, conversation_flow)
        bot_text += "".join([item["bot_text"] for item in state["messages"] if item.get("bot_text")])

        ConversationManager.update_trace(trace, next_state)

        return bot_text

    @staticmethod
    def start_conversation(conversation_flow, trace, user_text):
        """Starts the conversation"""

        # Welcome message
        data = {'state_name': 'start', 'user_text': '', 'context': {'lang': 'en'}}
        state = State.get_current_state_from_database(data, conversation_flow)
        bot_text = "".join([item["bot_text"] for item in state["messages"] if item.get("bot_text")])
        next_state = next(item["to_state"] for item in state["triggers"] if item.get("to_state"))

        # # First question
        data = {'state_name': next_state, 'user_text': user_text, 'context': {'lang': 'en'}}
        state = State.get_current_state_from_database(data, conversation_flow)
        bot_text += "".join([item["bot_text"] for item in state["messages"] if item.get("bot_text")])

        ConversationManager.update_trace(trace, state["state"]["state_name"])

        return bot_text
