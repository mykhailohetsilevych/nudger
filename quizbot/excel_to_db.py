import pandas as pd
from nudger.settings import BASE_DIR

df = pd.read_excel(
    f"{BASE_DIR}/data/num-one.xlsx",
    sheet_name="rori-demo",
    engine="openpyxl",
    header=None,
)

for index, row in df.iterrows():
    if not row[1] and not row[3]:
        break
    elif row[1] == "-" and row[3] == "-":
        break
    elif row[1] != "-":
        print(row[1])
        print(f"question: {row[3]} answer: {row[4]}")
    elif row[3] != "-":
        print(f"question: {row[3]} answer: {row[4]}")
