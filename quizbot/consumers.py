import asyncio  # noqa
import json

from channels.db import database_sync_to_async
from channels.generic.websocket import AsyncWebsocketConsumer
from django.core.exceptions import ObjectDoesNotExist

from quizbot.bot_manager import StateManager, START_STATE_NAME
from quizbot.models import Convo, Checkpoint
from quizbot.tasks import schedule_chat_message_2, send_out_chat_message_2
from users.models import User

RESPONSE_DELAY_PER_CHAR = 0.05
RESPONSE_DELAY_MIN = 0.2
DEFAULT_CONVO = "countByOne"  # Default bot for anonymous user


class QuizRoomConsumer(AsyncWebsocketConsumer):
    """ Manages channel interactions for users via ASGI. """

    def __init__(self, **kwargs):
        super(QuizRoomConsumer, self).__init__(**kwargs)
        self.room_name = None
        self.room_group_name = None
        self.room = None
        self.active_checkpoint = None
        self.username = None
        self.convo_name = None

    @database_sync_to_async
    def get_convo_name(self):
        try:
            user = User.objects.get(username=self.username)
            convo_name = user.profile.convo_name
        except ObjectDoesNotExist as err:
            return DEFAULT_CONVO
        else:
            return convo_name

    async def connect(self):
        """ Connect to a chat room and assign a user a group """
        self.room_name = self.scope['url_route']['kwargs']['room_name']
        self.room_group_name = f'chat_{self.room_name}'

        # User has to be logged in.
        self.username = self.scope["user"]
        self.convo_name = await self.get_convo_name()

        if not self.convo_name:
            await self.close()

        # Join room group
        await self.channel_layer.group_add(
            self.room_group_name,
            self.channel_name
        )

        # Accept the connection
        await self.accept()
        # Trigger sending the initial state

        await self.channel_layer.group_send(
            self.room_group_name,
            {
                'type': 'send_welcome_message',
                'welcome_text': 'hello world',
            }
        )

    async def set_interaction_data(self, data):
        """ Creates a dictionary with information that describes the context of the message being sent """

        interaction_data = {
            # message_id=data['message_id'], Automatically set
            'message_direction': data['message_direction'],
            'message_text': data['message_text'],
            # send_time=data['send_time'], Automatically set
            'state': data['state'],
            'sender_type': data['sender_type'],
            'user_id': data['user_id'],
            # 'bot_id': "Poly Chatbot",
            # 'channel': "WSNYC Website",
        }
        return interaction_data

    async def manage_message_queue(self, state_data):
        """ Receives full state data object, and schedules a celery worker to send out each part of the state
        Sends task 4 things: room_group_name (channels), message object, delay in seconds, dictionary of interaction data
        """
        delay_send_after_received = 0
        delay_diff = 3

        # Schedule_chat_message creates a task in celery_beat
        if state_data.get('user_context'):
            interaction_data = await self.set_interaction_data(
                data={
                    'message_direction': 'inbound',
                    'message_text': state_data['user_context']['user_text'],
                    'state': state_data['state']['state_name'],
                    'sender_type': 'user',
                    'user_id': self.room_group_name,
                }
            )

            schedule_chat_message_2.delay(
                self.room_group_name,
                {'user_text': state_data['user_context']['user_text']},
                delay_send_after_received,
                interaction_data,
                self.active_checkpoint.id
            )

        for message in state_data['messages']:
            state_data_copy = state_data.copy()
            message = dict(message)

            # delay_send_after_received += len(message['bot_text']) * RESPONSE_DELAY_PER_CHAR

            interaction_data = await self.set_interaction_data(
                data={
                    'message_direction': 'outbound',
                    'message_text': message['bot_text'],
                    'state': state_data['state']['state_name'],
                    'sender_type': 'chatbot',
                    'user_id': self.room_group_name,
                }
            )

            send_out_chat_message_2.delay(
                room_group_name=self.room_group_name,
                state_data={
                    'bot_text': message['bot_text'],
                    'triggers': state_data_copy['triggers'],
                    'state': state_data_copy['state']
                },
                interaction_data=interaction_data,
                checkpoint_id=self.active_checkpoint.id
            )

        if state_data.get('triggers'):
            # If a message has a trigger, we want to delay
            # delay_send_after_received += delay_diff

            interaction_data = await self.set_interaction_data(
                data={
                    'message_direction': 'outbound',
                    'message_text': message['bot_text'],
                    'state': state_data['state']['state_name'],
                    'sender_type': 'chatbot',
                    'user_id': self.room_group_name,
                }
            )

            send_out_chat_message_2.delay(
                room_group_name=self.room_group_name,
                state_data=state_data.copy(),
                interaction_data=interaction_data,
                checkpoint_id=self.active_checkpoint.id
            )

    async def send_welcome_message(self, event):
        """ Handles send_welcome_message event type

            Gets the welcome state data upon connect and sends the data to the message queue.
        """

        data = {
            'state_name': START_STATE_NAME,
            'user_text': '',
            'context': {'lang': 'en'}
        }
        state_data = await self.get_state_data(data)

        await self.manage_message_queue(state_data)

    async def disconnect(self, close_code):
        """ Leave the channel, room, and user group """
        await self.channel_layer.group_discard(
            self.room_group_name,
            self.channel_name
        )

    @database_sync_to_async
    def get_state_data(self, data):
        """ Calls State.get_next_state_from_database function to retrieve the next state data from the database """
        # package = NextStateReply(data)
        if not self.active_checkpoint:
            # Gets the active version from DB
            convo = Convo.objects.get(name=self.convo_name)
            self.active_checkpoint = Checkpoint.objects.get(
                convo=convo,
                published=True,
            )
        package = StateManager.manage_state(data, self.active_checkpoint)
        return package

    async def receive(self, text_data):
        """ Uses the received message JSON to get the next state's full data

        Returns the user_text (the user's selected response), the current state context, and the next state context.
        """
        text_data_json = json.loads(text_data)
        message = text_data_json['message']
        state_data = await self.get_state_data(message)

        # TODO: Can I remove "context"
        try:
            state_and_user_data = {
                "user_context": message,
                "state": state_data['state'],
                "messages": state_data['messages'],
                "triggers": state_data['triggers'],
                "context": state_data['context']
            }
        except KeyError:
            state_and_user_data = state_data

        try:
            if state_and_user_data['error_message']:
                await self.channel_layer.group_send(
                    self.room_group_name,
                    {
                        'type': 'chat_message',
                        'message': state_and_user_data
                    }
                )
        except KeyError:
            await self.manage_message_queue(state_and_user_data)
            # await self.manage_message_queue(state_and_user_data)

    async def chat_message(self, event):
        """ Handles the chat_message event type

        Sends out a message to the websocket after receive() """
        message = event['message']
        # Send message to WebSocket
        await self.send(text_data=json.dumps({
            'message': message
        }))
