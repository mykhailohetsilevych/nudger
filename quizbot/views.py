import json
import io
import urllib.request
import uuid
from datetime import datetime

import markdown
import yaml
from django.conf import settings
from django.contrib import messages
from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.shortcuts import render, redirect
from django.utils.html import format_html
from django.utils.safestring import mark_safe
from django.views.generic import ListView
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.views import APIView

from nudger.settings import MEDIA_URL, USE_S3, RENDER
from quizbot.api_manager import ConversationManager
from quizbot.forms import DocumentForm
from quizbot.load_yaml import create_flow_data
from quizbot.models import State, Document


def room(request, room_name):
    file_css = 'quizbot/bundle.css'
    file_js = 'quizbot/bundle.js'
    url = "wss://www.qary.ai/ws/quiz" if settings.RENDER else "ws://localhost:8000/ws/quiz"
    return render(request, 'quizbot/mathbot.html', {
        'room_name': room_name, 'file_css': file_css, 'file_js': file_js, "welcome_popup_on": json.dumps(False), "empty_message_allowed": json.dumps(False), "name": "Polly", "bot_picture": "https://gitlab.com/tangibleai/nudger/-/raw/main/static/sms_nudger/poly-bundle/poly-logo.png", "chat_widget_icon": "bi bi-robot", "widget_icon": "https://gitlab.com/tangibleai/nudger/-/raw/main/static/sms_nudger/poly-bundle/poly-transparent.png", 'ws_url': url
    })


def quiz(request):
    room_name = uuid.uuid4().hex
    return redirect('quizbot-room', room_name)


class APINextStateReply(APIView):
    """ Returns the next state based on the input

    Uses the State.get_next_state_from_database in models.py to find the next state information

    Use with /quiz/api

    JSON Input Example
    {
        'state_name': 'select-language',
        'user_text': 'English',
        'context': {'lang': 'en'}
    }

    Query Parameter Example
    /quiz/api/?state_name=selected-language-welcome&user_text=Ready
    """

    def get(self, request):
        data = request.data
        data.update(request.query_params.dict())
        time = datetime.now()
        message_id = uuid.uuid4()

        response = State.get_next_state_from_database(data)

        return Response({
            "state": response['state'],
            "messages": response['messages'],
            "triggers": response['triggers'],
            "context": response['context'],
            "message_id": message_id,
            "message_direction": "outbound",
            "sender_type": "chatbot",
            "user_id": "chat_j2Lk356",
            "bot_id": "Poly Chatbot",
            "channel": "WSNYC Website",
            "send_time": time

        })


@staff_member_required
def upload(request):
    if request.method == "POST":
        form = DocumentForm(request.POST, request.FILES)
        if form.is_valid():
            error_msg = ""
            try:
                content = request.FILES["file"].read()
                convo = yaml.safe_load(content)
                convo_name = convo[0].get("convo_name")
                convo_description = convo[0].get("convo_description")
            except Exception as err:
                error_msg = err
                convo_name, convo_description = None, None
            if convo_name and convo_description:
                instance = Document(file=request.FILES["file"])
                instance.convo_name = convo_name
                instance.description = convo_description
                instance.save()
                return redirect('quizbot-document-list')
            else:
                messages.info(
                    request,
                    format_html("{} {} {}",
                                error_msg,
                                "Could not extract convo name or description, use the format in the ",
                                mark_safe(
                                    '<b><a href="https://gitlab.com/tangibleai/nudger/-/blob/main/data/countByOne_0001.yml">link</a></b>'),
                                )
                )
                return redirect('quizbot-upload')

    form = DocumentForm()
    return render(request, 'quizbot/upload.html', {
        'form': form
    })


class DocumentListView(PermissionRequiredMixin, ListView):
    """Returns questions as list"""
    permission_required = 'is_staff'
    model = Document
    template_name = 'quizbotbot/document_list.html'
    context_object_name = 'documents'
    ordering = ['-created_on']  # '-' to descending
    paginate_by = 10

    def get_queryset(self):
        """Overriding query method"""
        return Document.objects.all().order_by('-created_on')


@staff_member_required
def create_flow(request):
    """Creates flow data using '.yml' files stored in media folder
    Changes active flow to the current flow
    """
    if USE_S3:
        # using S3 as an object storage
        url = f"{MEDIA_URL}{request.POST.get('file')}"
        response = urllib.request.urlopen(url)
        io_wrapper = io.TextIOWrapper(response, encoding='utf-8')
        result = create_flow_data(
            io_wrapper,
            convo_name=request.POST.get('convo_name'),
            description=request.POST.get('description')
        )
    else:
        # using local storage
        from nudger.settings import MEDIA_ROOT
        result = create_flow_data(
            file_path=f"{MEDIA_ROOT}/{request.POST.get('file')}",
            convo_name=request.POST.get('convo_name'),
            description=request.POST.get('description')
        )

    if result:
        # Displays info message on the web page
        messages.info(request, result)

    # Returns to the document list view
    return redirect('quizbot-document-list')


# @api_view(["POST", "GET"])
# def conversation(request, bot_name=None):
#     """Returns the answer of the question if exists in the database
#     else returns a static answer
#     """
#
#     if request.method == "GET":
#         return Response({"message": "Use a post method!"})
#
#     bot_name = bot_name
#     student_id = request.POST.get("student_id")
#     message = request.POST.get("message")
#
#     bot = ConversationManager.get_bot(bot_name)
#
#     if not bot:
#         message = markdown.markdown("Endpoint with that bot-name does not exist!", extensions=['attr_list'])
#         return Response({"message": message})
#
#     conversation_flow = ConversationManager.get_conversation_flow(bot, bot.version)
#
#     trace = ConversationManager.get_trace(student_id)
#
#     if not trace.state_name and message.lower() != "start":
#         message = markdown.markdown("Type start to begin quiz!", extensions=['attr_list'])
#         return Response({"message": message})
#     elif message.lower() == "start":
#         response = ConversationManager.start_conversation(conversation_flow, trace, "next_state")
#         return Response({"message": response})
#     elif "end" in trace.state_name:
#         message = markdown.markdown("This exercise has been finished!", extensions=['attr_list'])
#         return Response({"message": message})
#     else:
#         response = ConversationManager.update_state(conversation_flow, trace, "next_state", message)
#         return Response({"message": response})
