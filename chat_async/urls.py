# chat/urls.py
from django.urls import path

from . import views
from .views import (
    APINextStateReply,
)

urlpatterns = [
    path('api/', APINextStateReply.as_view(), name='next-state'),
    path('', views.redirectpage, name='redirectpage'),
    path('<str:room_name>/', views.room, name='room'),
]
