# Sprint 28 (Sep 30 - Oct 07)

### Backend (high priority)

* [x] R1-1.2: Take a refresher look at Tangible AI [security policy](https://gitlab.com/tangibleai/team/-/blob/main/handbook/security-policy.md) before you access MOIA data
* [x] R1-0.5: Have Maria grant you access to the MOIA data
* [x] R0.5-0.5: Enable 2FA for Bitwarden and your Tangible AI GMAIL address
* [x] R2-2: Generate the MOIA analysis report for September before next Wednesday
* [x] R1-1.5: Prepare for MOIA report presentation
* [x] R2-0.5: Expanding the Poly chain example to help you fix the OOO bug in real Poly
* [x] R1-1: Create a task to get the current state object
* [x] R1-0.5: Create a task to get the bot text messages
* [x] R1-0.5: Create a task to get the bot triggers
* [] <del>R1: Create a task to send user message (selected trigger)</del>
* [x] R2-1: Implement a chain that includes the tasks above
* [x] R1-0.5: Understand `apply_async()` and the attribute `countdown`
* [] <del>R1: Make the last task of the chain updates the current state object</del>

### Frontend (less priority)

* [] R1: Include user feedback in a new file called **feedback.md**
* [] R1: Add ellipsis spinner that loads continuously before every bot message
* [] R2: Improve the UI based on user feedback 

## Done: Sprint 27 (Sep 23 - Sep 30)

### Backend

* [x] R2-1.2: Ask some people for a feedback on old Poly on WeSpeakNYC and the new version on qary.ai
* [x] R1-1: Add a delay to each scheduled message that is dependent on the index variable i of the for loop
* [x] R1-1: Make sure all function calls within for loop are synchronous
* [x] R1-1: Add a minimum delay relative to the previous message of > than 0 for every message 
* [x] R2-1.5: Figure out an approach to fix the out-of-order bug
* [x] R0.2-0.2: Add the approach to the sprint plan 
* [x] R2-2: Create a simple example to learn/implement a Celery chain based on your approach
* [x] R2-0.5: Fix the bug you have in your simple Celery chain 
* [x] R1-1: Create another Celery chain that similates the state maintainance in Poly
* [x] R1-0.8: Share the examples with your team to get feedback 
* [x] R1-1: Add a delay to every task in the chain

### Frontend

* [x] R2-1: Move all widget elements to the front
* [x] R0.2-0.2: Create a new HTML file to make sure all the widget elements are in front


