# We Speak NYC Vendor Organization Scope of Work

## 1. Tangible AI, vendor, will:

```text
1. Tangible AI, vendor, will:
    a. Develop software to improve the usability, reliability and maintainability of the
    chatbot:
        i. Documentation of existing code
        ii. Automated testing
        iii. Manual testing
        iv. Automated deployment and monitoring features
        v. Add translated content (10 languages total)
            1. Translated content exists currently as data that needs to be coded into the website. Tangible Ai will be coding it to add language capacity to our chatbot.
        vi. Improve mobile-friendly chatbot frontend
        vii. Implement immigrant-friendly user experience improvements in consultation with MOIA
    b. Provide user interaction analytics information:
        i. Measure and monitor user engagement interaction
        ii. Provide web page for MOIA access to user interaction data
        iii. Report user engagement statistics for the period of September - December 2021.
        iv. Report user engagement statistics for the first 6 calendar months in 2022
    c. Provide WSNYC with monthly usage reports and analytics.
    d. Provide chatbot subscription services and ongoing maintenance according to Service Level Agreement specified in Attachment A of MOIA’s Cloud Agreement with Tangible AI, during the months of July - September 2022.
2. The City University of New York (CUNY), We Speak NYC (WSNYC) Team, will:
    a. Collaborate and advise with Tangible AI in WSNYC pedagogy taken from the popular education model and best practices for intermediate English language learners.DocuSign Envelope ID: B296750D-0D1F-4EE2-ABBE-555C42BAD141
```

### E. Reporting

```text
1. Tangible AI will provide monthly reporting based on usage of chatbot on WSNYC’s website. Reporting will include only aggregated anonymous statistics and no PII. Reporting will be provided no later than June 30th, 2022.
2. Reporting outcomes to MOIA and WSNYC, include but are not limited to the following:
    a. Successfully track 3 Journeys:
        i. User/ Student/ Learner to Services
            1. Measure number of referrals.
            2. Measure if user was able to access information about city services related to WSNYC episode content featured on the WSNYC website.
        ii. User/ Student/ Learner to English Language Materials and Conversation Classes (seasons and episode page with study guides and practice quiz button along with page to online courses)
            1. Measure number of referrals.
            2. Get direct feedback (reviews and comments) from users to measure if
            the resource was helpful.
        iii. Educators to Educational Materials, Lesson Plans, Synchronous and Asynchronous videos for Professional Development (webinar videos for educators, teaching and civic resources)
            1. Measure number of referrals
            2. Get feedback to measure if resource was helpful and if educators used any items in their classroom
```
