# Sprint 24 (Sep 02 - Sep 09)

> Chameleon (`faceofqary`)

* [x] R0.5-0.5: Estimate the effort/time you spent on the tasks for the sprint 4
* [x] R0.1-0.1: Rename the `new-ideas.md` file to `backlog.md`
* [x] R0.1-0.1: Move the `backlog.md` file to the `planning/` directory in `nudger` repo
* [x] R0.2-0.2: Make sure there are no tasks or bugs in more than one file
* [x] R0.1-0.1: Replace the name **Purple Alien** with **Poly**
* [x] R0.2-0.2: Replace the cat profile with Poly logo
* [x] R1-1.2: Watch the video again to clearly understand how to update the MOIA report
* [] R2: Try to update the MOIA report to include data up to Aug 31
* [x] R2-2.8: Continue learning and practicing the basics of Django channels/Web Sockets in the [learning repo](https://gitlab.com/rochdikhalid/django-channels)
    * [x] R1-0.5: Basic setup 
    * [x] R0.5-0.2: Create a template for room view
    * [x] R0.5-0.5: Write a consumer 
    * [x] R0.5-0.5: Enable the channel layer 
    * [x] R0.5-R0.1: Rewrite the chat server as asynchronous
    * [x] R1-1: Review and understand what I have learned about Django channels
* [x] R1.5-1: Understand how channels are implemented in `nudger`
* [x] R0.5-0.2: Read and understand what Celery is used for
* [x] R2-1: Practice Celery in the [learning repo](https://gitlab.com/rochdikhalid/dj-celery)
    * [x] R0.1-0.1: Create a new repository named dj-celery and clone it
    * [x] R0.1-0.1: Create a virtual environment and install dependencies
    * [x] R0.1-0.1: Create a Django project named simpletask
    * [x] R0.2-0.1: Incorporate Celery into the Django project
    * [x] R0.2-0.1: Configure Redis server
    * [x] R0.1-0.1: Create a new app called sendmessage
    * [x] R0.5-0.2: Create Celery tasks and test them
* [x] R1-0.5: Fix the bug that causes the appearance of Poly triggers (buttons) before the bot text
* [x] R1-1: Test manually the bug fix locally and in production
* [x] R0.1-0.1: Estimate the points on the tasks you've put in the sprint plan 5

> Learning NLP with **NLPiA**

* [x] R1-2: Add some learning tasks to read chapter 2 in **NLPiA**
    * [x] R2-1.5: Read and understand **2.2 Building your vocabulary with a tokenizer**
    * [x] R1-0.5: Read and understand **2.2.1 Dot product**
* [x] R1-1.5: Do an exercise to create BOW vectors for each document in the MAITAG (Maya) dataset of anonymized utterances
* [] H?: Create tokenization exercise instructions on how to use ipython
* [] H?: Check the answer with doctest and use hist to export your work
* [] H?: Create a vectorization exercise
* [] H?: Create `team/exercises/nlpia/` dir for all the NLP exercies

## Done: Sprint 23 (Aug 26 - Sep 02) 

> Main goal: complete the frontend
 
* [x] R2-2: Search for some online chatbot examples 
* [x] R1-1: List the new features/improvements that will be implemented in a file
* [x] R1-0.7: Add an error message when the user input doesn't match one of the listed options
* [x] R0.1-0.1: Add styling to the error message
* [x] R0.1-0.1: Change the style of the inout field when the error shows up
* [x] R0.5-0.2: Improve the style of the chat triggers (buttons)
* [x] R0.5-0.2: Resize the chat components to fit the new look of chat triggers
* [x] R0.2-0.1: Make the new triggers changes responsive to small screens (mobile, etc)
* [x] R0.2-0.2: Add a list menu to the chat header 
* [x] R0.1-0.2: Add stying to the list menu
* [x] R1-0.7: Close the menu when you click on an item or outside the menu
* [x] R0.5-0.2: Write a function that gets the "reset chat" item working
* [x] R0.5-0.2: Write a function that gets the "exit chat" item working
* [x] R0.2-0.1: Fix the noHistory div display
* [x] R1-1: Meet Maria to get an overview about the MOIA's monthly report (August, 2022)
* [x] R1-1: Do research on how "exit chat" works in other chat widget examples
* [x] R0.5-0.2: Make a spinner that loads before the conversation started
* [x] R0.1-0.1: Add styling to the spinner
* [x] R0.2-0.5: Fix the height/max-height of the chat room component
* [x] R0.2-0.2: Reset the input field and the error message when the you reset the conversation
* [x] R0.1-0.1: Fix the width of the chat room component
* [x] R1-0.5: Add three new configurations to the settings file and get the working
* [x] R2-1: Make the triggers clickable and maintain the current state
* [x] R1-0.5: Remove the triggers of the current state when one option is selected