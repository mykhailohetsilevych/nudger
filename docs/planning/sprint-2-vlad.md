# Python sprint plan 3

Apr 24

- [] create new section in README of how to build new bundle, how to copy it over nudger (https://gitlab.com/tangibleai/community/faceofqary)
- [] remove the requirement for buttons in bundle.js on face of Qary (https://gitlab.com/tangibleai/community/faceofqary)
- [] redeploy and test it on Qary.ai
- [] help Greg create a copy of wormhole on Render
- [] start migrating mathtext-fastapi to django
- [x] create '3-django-javascript' in https://gitlab.com/tangibleai/community/team/-/tree/main/exercises, create README.md
- [] create assets folder in nudger creating the bundle.js

Apr 17

- [x] get familiar of how to use package manager, bunder and compiler (transpiler) with javascript
- [x] go through "Modern JavaScript for Django Developers" tutorial
- [x] how to use npm to create build.js
- [x] get more familiar with Turn.io

Apr 10

- [x] fix errors with mathactive:
  - [x] wrong workflow: precalculation of next skill score. When user gives wrong answer he should gain simpler question, but things go vice versa
  - [x] fix wrong numbers in data spreadsheet
  - [x] connect Sentry
  - [x] create module to load environment variables and store them in dictionary to avoid side effects
- [] Delvin:
  - [x] style register and login pages with bootstrap
  - [] as we use custom styles provide custom validation to form fields

Apr 3

- [] make work quiz for any positive start, stop and step

```
from logging import getLogger
log = getLogger(__name__)
log.warning("Wrong step size")  # log.error, log.info, log.debug
```

- [] edge cases:
  - when user passes invalid stop value we need to calc it with start + 3 \* step
  - step < 0: `abs()` + change the question
  - step = 0 -> bug -> step = 1, log an error message, use logging function, ask Greg about Sentry.io
  - stop >> start:
  ```def start_stop_step_to_num_steps(start, stop, step):
        if num_steps > 10:
          # >>> start_stop_step_to_num_steps(1, 11, 1)
            10
          # >>> start_stop_step_to_num_steps(1, 12, 1)
            11
  ```
  ```
  def generate_question(start, stop, step=1, num_steps=None):
    num_steps = start_stop_step_to_num_steps(1, 12, 1)
    if num_steps > 10:
      generate_question
      # change message text to 1, 2, 3, ... 9, 10, 11
  ```

Mar 27

- [x] tanibleai/delvin:
  - [x] help Cetin building dashboard (html, css, js)
  - [x] fancy navigations bar powered by styles and javascript
  - [x] add ability to select date range
  - [x] send dates selected by user to Back-End

Mar 20

- [x] use local file in place of "database" to store/update users' data
- [x] let all the interaction with quiz be carried through one endpoint called '/num_one'
- [x] endpoint-handling function should call `process_user_message` main function accepting user_id as required parameter and message_text + state as optional ones

Mar 13

- [x] move all the functions from mathtext-fastapi to mathactive repository. This way only endpoints would be utilized in mathtext-fastapi
- [x] in a data spreadsheet use 4 columns instead of 2: difficulty, start, stop, step to let us choose between sequences of 1, 2 or 3 numbers when asking user a question
- [] create a user spreadsheet with id, current_skill_score columns to store tied to user information and use this info in my functions by letting pass it as an argument (id, current_skill_score) - need to be detailed regarding way of storing it (either in a database or )
- [] take a look at Hobs's code to store users' data in RAM rather than on disk (you said that's an option for us as long as a database) and try to copy your approach into mine

Mar 6

- [x] create a csv file that contains start, stop and step values
- [x] create a data-driven algorithm that generates start, stop, step depending on difficulty level

Feb 27

- [x] finish with work we started (use exact the same questions from spreadsheet)
- [x] create a new pair of endpoints:
      1 - generate one of 7 different question forms(we should be able to provide keyword argument matching number of question, default = 1)
      2 - generates a sequence of numbers joined by commas. Reminder:
      `"{', '.join([str(num) for num in range({start}, {stop})])}".format(start=8, stop=11)`
- [x] ability to specify the score between 0 (easiest) and 1(hardest):
      easy <= 0.3
      hard >= 0.6
- [] try to make more of data-driven code (load from a csv and use ) # under the question

Feb 20

- [x] create an API in huggind face (using FastAPI)
- [x] create API endpoints for 6 functions in questions.py
- [x] install Whatsapp
- [x] remind Hobson to send me an invitation to Rori.ai on Whatsapp
- [x] play with Rori.ai
- [x] -H invite to tangible ai team and push a mathactive repository to hugging face
- [x] try to make my bot more like Rori

Feb 13

- [x] meld models.py for chat-async and quizbot
  - [x] manually test at qary.ai
  - [x] make small code change, approximately 10 lines of code
  - [x] create merge request
  - [x] manually test at qary.ai
  - [x] rinse and repeat

# Python sprint 1

Feb 6

- [] start.sh (run scrip to launch an app)
- [] use gunicorn
- [] try start.sh script from nudger repo (asynchronous approach)
- [] models.py file to look for state
- [] https://gitlab.com/tangibleai/nudger/-/blob/main/chat_async/models.py#L206 - refactor function to have it working
- [] H invide Vlad to nudger
- [] play with qary.ai
- [] create branch vlad on nudger
- [] deploy vlad branch to render

## check out stigsite

- [] read deploy.md from stigsite
- [] watch 2 videos from Hobson
- [] research cryptobrokers/banks/exchanges in Ukraine

  Jan 30

- [x] create render.com account
- [x] create supabase account
- [x] fix bugs in my lannister program
- [x] deploy to render.com
- [x] connect lannister to supabase database - still in progress

## Backlog

- [] create table view of list of users
- [] make the table view sortable by any column
- [] create a search box for the table view with full-text search
- [] only admin can access table
- [] install django degub toolbar
- [] install django extensions
- [] python manage.py shell_plus
- [] add a fifth argument `num_steps=3` # num quiz generate_start_stop_step func
