# Sprint 32 (Oct 28 - Nov 04)

* [x] R1-0.5: Understand the Runtime error **Event loop is closed** 
* [x] R1-0.2: Fix the Runtime error **Event loop is closed**
* [x] R1-1: Run Flower dashboard locally to monitor Celery tasks
* [x] R2-2: Understand why the problem of repeated triggers and messages occurs
* [x] R1-1: Fix the problem of repeated triggers and messages
* [x] R2-1.5: Gnerate MOIA report analysis for October and all time
* [x] R0.5-0.2: Use the new web socket URL to general new links for the chat widget
* [x] R1-1: Take a quick tutorial on Django Extensions
* [x] R1-1: Take a quick tutorial on Django Debug Toolbar
* [x] R1-0.8: Diagnoze the out of order bug locally with Flower

## Done: Sprint 31 (Oct 21 - Oct 28)

* [x] R2: Get the Poly widget working in qary.ai home page (related to the tasks below)
    * [x] R: Create a PostgreSQL database on Render to handle concurrency
    * [x] R: Change the `./start-celery.sh` commands to the new command
    * [x] R: Test the widget in production to see if messages show up
* [x] HR1: Read and edit the proposal draft by adding the new upcoming features
* [x] R1-0.5: Add the database models of `chat-async/` to `poly/chat` app
* [x] R1-0.8: Setup Poly conversation data locally and in prodcution
* [x] R0.5-0.8: Deploy/test the admin page and database in production
* [x] R0.5-0.5: Configure the project settings to use web sockets and channels
* [x] R0.5-0.2: Configure the project settings to use Celery 
* [x] R1-0.2: Integrate the `ChatConsumer()` class into `poly/chat` consumers
* [x] R0.5-0.4: Add room templates to the app
* [x] R0.5-0.1: Add `chat-async` Celery tasks to the `poly/chat` app
* [x] R0.5-0.5: Run and test the integration locally
* [x] R1-0.5: Create a Celery background worker that will process Celery tasks
* [x] R1-0.5: Render Redis instance as the Celery broker 
* [x] R1-0.2: Run Flower with Celery to monitor Celery tasks
* [x] R1-0.5: Deploy Poly to wespeaknyc.qary.ai