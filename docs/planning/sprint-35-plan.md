# Sprint 35 (Nov 18 - Nov 25)

## MOIA 

* [x] R?: Add a new parameter named **ws_url** to the `settings.json` file
* [x] R?: Modify the `bundle.js` file to read the **ws_url** parameter from `settings.json`
* [x] R?: Make the `settings.json` file global to be modified by external apps
* [x] R?: Ensure the `settings.json` is working globally by changing the actual parameters
* [x] R?: Fix the bug **Cross-Origin Request Blocked** 

### Done Sprint 34 (Nov 11 - Nov 18)

* [x] R0.1-0.1: Create a branch called `fix-outoforder` to fix the OOO bug
* [x] R2-2.5: Take a tutorial to learn how to create an integration test
* [x] R2-2: Create a simple integration test for MOIA to be tested automatically after every deployment (check your notebook for more details) 
* [x] R1-1: Incorporate the `poly_api` app into the secure branch 
* [x] R0.2-0.1: Rename the `poly_api` to `convoscript`
* [x] R0.2-0.1: Rename the `poly` to `convosite`
* [x] R1-0.5: Fix the `QaryMessage()` validation and JSON serialization issues
* [x] R1-0.5: Remove delay from the consumer
* [x] R1: Check for the sequence number of messages in the frontend
* [x] R1: Use the sequence number to display messages in correct order
* [x] R1: Simplify the ChatConsumer class to make readable by having less code 
* [x] R0.2-0.1: Create an app called `monitor` 
* [x] R1-0.2: Create an HTML template and view for the PDF report
* [x] R1-0.5: Embed the MOIA report of October into the view `wespeaknyc.qary.ai/monitor`
* [x] R0.5-1: Merge the new changes of `fix-outoforder` into the `secure` branch
* [x] R2-2: Add some improvments the chat messages flow and update the Svelte bundle