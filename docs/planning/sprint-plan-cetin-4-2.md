# Quizbot Task Ideas

- [X] change models.CF.bot_name to CF.name
- [ ] new schema that has bot_name -> flow_name == ConversationFlow.name
- [X] within all dialog yaml files and database tables delete all WeSpeak and Language Selection dialog
- [X] fix hardcoded dialog_tree_filepath
- [X] button added to file list that can upload yaml and add to database
- [X] store objects in Object Storage system:
    1. Render Object Storage
    2. Digital Ocean Spaces
    3. A CDN (Content Delivery Network) provider like CloudFront?
    3. AWS S3

#### Optional Teaching/Mentoring Help

- [X] review team/exercises/2-mathtext/README.md
- [X] edit/correct/improve the README.md or add tests to the test files
- [ ] review Rori text normalization tasks
- [ ] create similar exercise for another one of our RORI tasks


#### *`quiz.yml`* (example start state)
```yaml
-
  name: start
  level: 1
  actions:
    en:
      - Welcome to Quiz Bot!
      - Let's get started with a math question to measure your math skill.
      - What is 1+1?
-
  name: 2
  ...
```


### Advanced Idea 1 - upload file form
- default flow_name is the filename
- parse the filename to remove extension `Path(p).with_suffix('').name`
- replace all punctuation (`_`) with `-`
- in upload yaml file form the filename different from bot_name
- prepopulate bot name field with file name with spaces in place of hyphens spaces and Path(p).with_suffix('')

### Advanced Idea 2 - upload file form 
- if not conversation_flow then need to overwrite existing database records
- DBeaver database admin interface


## Git Workflow

1. edit dev branch files
2. git commit, push
3. merge to main
4. git checkout main
5. git pull main
6. git checkout dev-branch
7. git merge main
8. edit dev branch files
9. git commit, then push
10. create new merge request or leave it open

## RORI Task Ideas

SoundEx is a package for encoding what words sound like.


5.2. NLP Features
5.2.1. PoC API+TurnIO architecture (see architecture diagram)

5.2.2. Basic Math NLU
Fuzzy matching of math answers and triggers (correct and incorrect answer categories or intents) or spelling correction (normalization) of natural language numbers: 

a. homophone detection: {“thirty to”, “tertyto”, “dirty too”} => “thirty two” 
b. typo correction: {“1 2”, “twleve”} => “twelve”
c. number representation folding: {“1,234”, “1_234”, “1.2340e3”} => “1234”
d. short number words (semantic folding): {“four”} => “4”  and  {“twelve”} => “12”

5.2.3. Improve keyword NLU
Semantic intent recognition (intent categories for each keyword)  

a. keyword synonym/semantic folding: {“quit”, “bye”, “cya”, “gotta go!”} => “exit”
b. number phrases: {“one thousand two hundred & thirty-four”} => “1234”

5.2.4. Sentiment Analysis
Add sentiment analysis to NLU API

a. sentiment polarity: 
{“I get it!”} => {“positivity”: .9, “intensity”: .6}
{“i get it”} => {“positivity”: .9, “intensity”: .2}
{“COOL!”} => {“positivity”: .95, “intensity”: .95}

b. learning/understanding: 
{“i dont understand”} => {“mastery”: .15}
{“WUT”} => {“mastery”: .05}

c. rapport:
{“u dumb”} => {“thanks”: 0.05, “like”: 0.1, “intensity”: .5}
{“you’re cool”} => {“thanks”: 0.8, “like”: .9, “intensity”: .8}
{“thank you”} => {“thanks”: 0.8, “like”: .9, “intensity”: .5}
{“you rock!”} => {“thanks”: 0.95, “like”: .95, “intensity”: .9}

d. motivation/morale:
{“gosh this is hard”} => {“discouraged”: .85, “intensity”: .8}
{“I’m dumb”} => {“discouraged”: .85}
