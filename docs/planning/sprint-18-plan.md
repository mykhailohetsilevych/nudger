# Sprint Plan

## Sprint 18: July 25 - July 31

### Hobs

- [ ] nudger cicd node.js install
- [ ] auto-deploy main or staging to render

### Greg

* [ ] GR1: Overview of MOIA/Nudger (30 min) > Solve a problem together (30 min)
      - mention two problems to Rochdi (early) > work on these in meeting
* [ ] G4: Selenium visits /chat-async/testroom/ and submits "English" and the new state is returned and the new state elements are not in the html that Selenium receives/picks up
* [ ] G8: The user visits /chat-async/testroom/ and new text appears correctly, but when they refresh the page, the chatbot stops working (blank except the input field) 
      - need to store state/context in a line of Javascript
* [ ] G2: Document edge cases
* [ ] HG2: Do research on pure Django tests for channels
      - Write most basic test possible for channels that doesn't require Selenium
* [ ] G1.5: Upgrade/create docstring about 10 important functions
      - """ Render view of index.html template for choose_room textbox """
* [ ] G8: Delay messages so they come across at a more natural pace
      - hard code a fixed # of seconds delay
      - hard code a fixed # of seconds per char delay (0.1)
      - create kwarg with default value of 0.1 in function/class init
      - (not get to) add to v4.yml default meta or input
      - read that during the processing of the yml and change kwarg value as you instantiate the consumer

## Done

### Sprint 17: July 18 - July 24
* [X] G2-3.5: Complete entire tutorial
* [X] G2-3: Incorporate the styling (change templates)
* [X] G6-5: Change the consumers (ie., populate more than message text)
* [X] G8- 9.5: Incrementally work towards building what is needed for Poly (from tutorial) 
  - build out the json packet / make the response the same as the one responding to the channels
* [X] G2.5: Add Selenium tests
* [ ] Refine the analytics
