conda activate nudger
conda install django jupyter
conda install -c conda-forge django-environ
conda install -c conda-forge django-extensions
conda install -c conda-forge django-crispy-forms
conda install -c conda-forge sendgrid twilio
python manage.py runserver
